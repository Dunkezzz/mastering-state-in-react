import React, { useEffect, useState } from 'react';

const CommunityCard = () => {
  const [communityData, setCommunityData] = useState([]);
  const [showContent, setShowContent] = useState(true);

  useEffect(() => {
    fetch('http://localhost:3000/community')
      .then((response) => response.json())
      .then((data) => setCommunityData(data));
  }, []);

  const avatarCards = communityData.map((person, index) => (
    <div key={index} className={`card-${index + 1}`}>
      <img
        src={person.avatar}
        alt={`${person.firstName} ${person.lastName}'s avatar`}
        onLoad={() => console.log(`Avatar image loaded for ${person.firstName} ${person.lastName}`)}
      />
      <p className="first-name">{person.firstName}</p>
      <p className="last-name">{person.lastName}</p>
      <p className="position">{person.position}</p>
    </div>
  ));

  const toggleContent = () => {
    setShowContent(!showContent);
  };

  return (
    <div>
      <button onClick={toggleContent}>{showContent ? 'Hide section' : 'Show section'}</button>
      {showContent && (
        <div>
          <div className="big-com">
            <h2>Big Community of People Like Youuu</h2>
            <p>We’re proud of our products, and we’re really excited when we get feedback from our users.</p>
          </div>
          <div id="avatar-container">{avatarCards}</div>
        </div>
      )}
    </div>
  );
};

export default CommunityCard;
