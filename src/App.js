import './App.css';
import CommunityCard from './CommunityCard';
import JoinOurProgram from './JoinOurProgram';

function App() {
  return (
    <div className="App">
      <CommunityCard />
      <JoinOurProgram />
    </div>
  );
}

export default App;