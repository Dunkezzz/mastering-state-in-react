import React, { useState } from 'react';
import { validate } from './email-validator.js';

function JoinModule() {
  const [email, setEmail] = useState('');
  const [subscribed, setSubscribed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  function sendEmail(email, endpoint) {
    setIsLoading(true);
    const xhr = new XMLHttpRequest();
    const url = `http://localhost:3000/${endpoint}`;
    const payload = JSON.stringify({ email: email });

    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');

    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          const response = JSON.parse(xhr.responseText);
          if (response.success) {
            setSubscribed(endpoint === 'subscribe');
          }
        } else if (xhr.status === 422) {
          const response = JSON.parse(xhr.responseText);
          if (response.error === 'Email is already in use') {
            window.alert(response.error);
          }
        }
        setIsLoading(false);
      }
    };
    xhr.send(payload);
  }

  function handleEmailChange(event) {
    setEmail(event.target.value);
  }

  function handleSubscribeClick() {
    if (validate(email)) {
      sendEmail(email, 'subscribe');
    } else {
      console.log('Invalid email');
    }
  }

  function handleUnsubscribeClick() {
    sendEmail(email, 'unsubscribe');
  }

  return (
    <div className="join-container">
      <h2>Join Our Program</h2>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae !</p>
      <input
        type="email"
        placeholder="Email"
        value={email}
        onChange={handleEmailChange}
      />
      {subscribed ? (
        <button
          className="btn"
          disabled={isLoading}
          onClick={handleUnsubscribeClick}
          style={{ opacity: isLoading ? 0.5 : 1 }}
        >
          Unsubscribe
        </button>
      ) : (
        <button
          className="btn"
          disabled={isLoading}
          onClick={handleSubscribeClick}
          style={{ opacity: isLoading ? 0.5 : 1 }}
        >
          Subscribe
        </button>
      )}
    </div>
  );
}

export default JoinModule;
